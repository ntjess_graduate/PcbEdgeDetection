function handle = doubleMontage(cells, borderWid, borderIntensity, outName)
if nargin < 2
  borderWid = 0;
end
if nargin < 3
  borderIntensity = 255;
end
origClass = class(cells{1});

numCells = length(cells);
N_cell = ceil(sqrt(numCells));
M_cell = ceil(numCells/N_cell);
cells = cellfun(@(cell) normalize255(cell, true), cells, 'uni', false);
if mod(numCells, N_cell) ~= 0
  cells(end+1:N_cell*M_cell) = repmat(...
    {cast(ones(size(cells{1}))*borderIntensity, origClass)},...
    1,N_cell*M_cell - numCells);
end
for ii = 1:N_cell*M_cell
  [M, N, channels] = size(cells{ii});
  cells{ii} = [cells{ii}, ones(M, borderWid, channels)*borderIntensity];
  N = N + borderWid;
  cells{ii} = [cells{ii}; ones(borderWid, N, channels)*borderIntensity];
  M = M + borderWid;
end
% Resize all ims to smallest size
numEls = cellfun(@(cell) numel(cell(:,:,1)), cells);
[~,idx] = min(numEls);
[M,N, ~] = size(cells{idx});
for ii=1:length(cells)
  %   curCell = cells{ii};
  %   [curM, curN, ~] = size(cells{ii});
  %   MMargin = round((M-curM));
  %   NMargin = round((N-curN));
  %   curCell = padarray(curCell,[MMargin, NMargin], borderIntensity);
  %   cells{ii} = curCell;
  cells{ii} = imresize(cells{ii}, [M, N]);
end

% Square-ify matrix
out = reshape(cells,N_cell,M_cell);

out = cell2mat(out');
out = imresize(out, 0.5);
% Eliminate excess padding
out(:, end-borderWid+1:end) = [];
out(end-borderWid+1:end, :) = [];
if nargout > 0
  handle = imshow(out);
end
if nargin > 3
  imwrite(out, outName);
end
end