% Takes an image with intensities from [mi..ma] (min/max) and scales it to
% be in the range [0..255]. Optionally converts the output to uint8
function out = normalize255(ims, toUint8)
if nargin < 2
  toUint8 = false;
end
if ~iscell(ims)
  out = performOp(ims, toUint8);
  return
end
out = cell(size(ims));
for ii = 1:length(ims)
  img = ims{ii};
  img = performOp(img, toUint8);
  out{ii} = img;
end
end

function img = performOp(img, toUint8)
origClass = class(img);
img = double(img);
mi = min(img(:));
ma = max(img(:));
img = (img - mi)*255/(ma - mi);
if toUint8
  img = uint8(img);
else
  img = cast(img, origClass);
end
end