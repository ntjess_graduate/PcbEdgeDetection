allIms = dir('./Imgs/*.png');
allIms = {allIms.name};

figToPrint = 'IDT4.png';

for ii = 1:length(allIms)
    imName = allIms{ii};
%   imName = 'TL_4.png';
  if strcmpi(imName, figToPrint)
    printfig = @(data, name) wrapImwrite(data, ['./Report/' name]);
  else
    printfig = @(data, name) false;
  end
  
  img = imread(['./Imgs/' imName]);
  
  % First, remove high frequency content, since in our case this will
  % mostly be unwanted noise
  if size(img, 3) > 1
    if contains(imName, 'TL')
      [r, g, b] = imsplit(double(img));
      % TL chips have red text, so emphasize this in grayscale construction
      grayIm = 2*r + g - 2*b;
      grayIm = uint8(rescale(grayIm)*255);
    else
      grayIm = rgb2gray(img);
    end
    printfig(grayIm, 'grayIm.png');
  else
    grayIm = img;
  end
  filImg = noiseFilter(grayIm);
  printfig(filImg, 'lowpassImg.png');
  
  % Locate chip on the paper
  chipMask = bwChipMask(filImg, printfig);
  
  % Project requirement part 1: Get chip outline
  chipOutline = edge(chipMask);
  printfig(imdilate(chipOutline, ones(3)), 'chipOutline.png');
  
  % Now find text on the board. We don't care about the background any
  % more, so crop the region of interest to the board dimensions
  chip = zeros(size(grayIm), 'uint8');
  chip(chipMask) = grayIm(chipMask);
  
  % Experimentation indicates median filtering removes much unwanted noise
  %   chip = colfilt(chip, [5 5], 'sliding', @median);
  threshChip = chip > 80;
  threshChip = imclose(threshChip, strel('disk', 8));
  % Each image contains white pins, so throw away this region
  lettersAndPins = bwconncomp(threshChip);
  lettersAndPins = lettersAndPins.PixelIdxList;
  compLens = cellfun('length', lettersAndPins);
  % Also assume all letters must be larger than 150 pixels in area
  letters = lettersAndPins(compLens < max(compLens) & compLens > 1800);
  
  % Create new bounding boxes for these letters
  letterOutlines = false(size(threshChip));
  for jj = 1:length(letters)
    letterOutlines(letters{jj}) = true;
  end
  letterOutlines = edge(letterOutlines);
  
  chipClr = [0 255 0];
  letterClr = [255 0 0];
  displayImg = img;
  for channel = 1:size(displayImg, 3)
    tmp = displayImg(:,:,channel);
    tmp(~chipMask) = 0;
    displayImg(:,:,channel) = tmp;
  end
  displayImg = overlayPeaks(imdilate(chipOutline, ones(3)), displayImg, chipClr);
  displayImg = overlayPeaks(letterOutlines, displayImg, letterClr);
  printfig(displayImg, 'pretilt.png');
  displayImg = correctTilt(displayImg, chipOutline, printfig);
  imwrite(displayImg, ['./Report/Output/' imName]);
end

function ret = wrapImwrite(data, name)
ret = true;
imwrite(data, name);
end