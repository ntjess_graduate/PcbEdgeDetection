%% Histogram of text location
load txtregion
txtSpread = histcounts(chip(txtregion), 256);
bar(txtSpread);
avgTxtVal = find(txtSpread == max(txtSpread));

hold off
chiphist = histcounts(chip(chipMask), 256);
chiphist = chiphist*100/sum(chiphist);
bar(chiphist);
hold on;
scatter(avgTxtVal, chiphist(avgTxtVal), 500, 'r', 'linewidth', 3);
title('Text intensity vs. overall chip intensity histogram');
xlabel('Intensity value');
ylabel('% occurrence');
ylim([0 2]);
xlim([0 256]);
export_fig ./Report/badChipTxtVals.png