function img = noiseFilter(img)
% Removes high-frequency noise
if size(img, 3) > 1
  img = rgb2gray(img);
end
freqImg = fftshift(fft2(img));
imgsz = size(freqImg);
diskmask = fspecial('gaussian', imgsz, 50);
img = real(ifft2(fftshift(freqImg.*diskmask)));
mi = min(img(:));
ma = max(img(:));
img = uint8((img - mi)*255/(ma - mi));
end